<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Rest extends REST_Controller {

    function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    //Menampilkan data intersection
    function index_get() {
	$type=$_GET['type'];
	
		if ($type=='a') {
			$id = $this->get('id_section');
			if ($id == '') {
				$intersection = $this->db->get('section')->result();
			} else {
				$this->db->where('id_section', $id);
				$intersection = $this->db->get('section')->result();
			}
			$this->response($intersection, 200);
		}
		else if ($type=='b') {
			$id = $this->get('id_phase');
			if ($id == '') {
				$intersection = $this->db->get('phase')->result();
			} else {
				$this->db->where('id_phase', $id);
				$intersection = $this->db->get('phase')->result();
			}
			$this->response($intersection, 200);
		}
    }

    //Input data intersection
		function index_post() {
        $data = array(
                    'name_section'           => $this->post('name_section'),
                    'id_status'          => $this->post('id_status'),
                    'id_phase'          => $this->post('id_phase'),
                    'id_setup'    => $this->post('id_setup'));
        $insert = $this->db->insert('section', $data);
        if ($insert) {
            $this->response($data, 200);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }
	
	//Update data intersection
	function index_put() {
        $id_section = $this->put('id_section');
        $data = array(
                    'name_section'          => $this->put('name_section'),
                    'id_status'          => $this->put('id_status'),
                    'id_phase'          => $this->put('id_phase'),
                    'id_setup'    => $this->put('id_setup'));
        $this->db->where('id_section', $id_section);
        $update = $this->db->update('section', $data);
        if ($update) {
            $this->response($data, 200);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }
	
	//Hapus data intersection
	function index_delete() {
        $id_section = $this->delete('id_section');
        $this->db->where('id_section', $id_section);
        $delete = $this->db->delete('section');
        if ($delete) {
            $this->response(array('status' => 'success'), 201);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }
	
}
?>